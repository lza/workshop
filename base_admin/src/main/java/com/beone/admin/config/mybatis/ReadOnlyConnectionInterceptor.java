package com.beone.admin.config.mybatis;

import com.base.common.datasource.DbContextHolder;
import com.base.common.datasource.ReadOnlyConnection;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * @title   Spring AOP切面类 拦截service层有 @ReadOnlyConnection 注解的方法
 * @Author 覃球球
 * @Version 1.0 on 2017/12/6.
 * @Copyright 长笛龙吟
 */
@Aspect
@Component
public class ReadOnlyConnectionInterceptor implements Ordered {

    public static final Logger logger = LoggerFactory.getLogger(ReadOnlyConnectionInterceptor.class);

    @Around("@annotation(readOnlyConnection)")
    public Object proceed(ProceedingJoinPoint proceedingJoinPoint,ReadOnlyConnection readOnlyConnection) throws Throwable {
        try {
            logger.info("set database connection to  only");
            DbContextHolder.setDbType(DbContextHolder.DbType.SLAVE);
            return proceedingJoinPoint.proceed();
        }finally {
            DbContextHolder.clearDbType();
            logger.info("restore database connection");
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
