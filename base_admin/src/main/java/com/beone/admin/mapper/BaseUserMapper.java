package com.beone.admin.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.base.SuperMapper;
import com.beone.admin.entity.BaseUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 运维数据_系统用户信息表 Mapper 接口
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BaseUserMapper extends SuperMapper<BaseUser> {

    /**
     * 根据用户名获取后台管理用户信息
     * @param name
     * @return
     */
    @Select("SELECT user_id AS userId, user_account AS userAccount, user_pwd AS userPwd, user_name AS userName" +
            ", user_sex AS userSex, user_mobile AS userMobile, idcard, user_status AS userStatus" +
            ", create_time AS createTime, last_visit AS lastVisit, lock_time AS lockTime" +
            ", error_number AS errorNumber, is_admin AS isAdmin FROM base_user  " +
            "  WHERE user_status='01' AND user_account = #{name}")
    BaseUser loadUserByUsername(String name);

    /**
     * 分页查询后台用户新
     * @param rowBounds
     * @param wrapper
     * @return
     */
    List<BaseUser> selectUserPage(RowBounds rowBounds, @Param("ew") EntityWrapper<BaseUser> wrapper);
}
