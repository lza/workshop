package com.beone.admin.utils.result;

/**
 * @Title
 * @Author 庞绪 on 2018/5/15.
 * @Copyright ©长沙科权
 */
public class Params {

    private String data;

    private String key;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Params{" +
                "data='" + data + '\'' +
                ", key='" + key + '\'' +
                '}';
    }

}
